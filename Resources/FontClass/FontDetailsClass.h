
//
//  Created by Rahul Sharma on 7/28/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#ifndef FontDetailsClass_h
#define FontDetailsClass_h

#define HelveticaNeueRegular                                 @"HelveticaNeue-Regular"
#define HelveticaNeueMedium                                @"HelveticaNeue-Medium"
#define HelveticaNeueThin                                       @"HelveticaNeue-Thin"
#define HelveticaNeueLight                                      @"HelveticaNeue-Light"
#define HelveticaNeueBold                                       @"HelveticaNeue-Bold"

#define HelveticaNeueMedium                         @"HelveticaNeue-Medium"
#define HelveticaNeueBold                         @"HelveticaNeue-Bold"
#define HelveticaNeueRegular                         @"HelveticaNeue-Regular"
#define HelveticaNeueLight                         @"HelveticaNeue-Light"
#define HelveticaNeueThin                         @"HelveticaNeue-Thin"



#endif

