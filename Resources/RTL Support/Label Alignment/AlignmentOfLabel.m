//
//  AlignmentOfLabel.m
//  Jaiecom
//
//  Created by Rahulsharma on 03/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "AlignmentOfLabel.h"


@implementation AlignmentOfLabel

- (id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    [self setAlignmentForLabel];
}
- (void)setAlignmentForLabel {
    
    if([[RTL sharedInstance] isRTL]) {
       [self setTextAlignment:NSTextAlignmentRight];
    }
    else {
        [self setTextAlignment:NSTextAlignmentLeft];
    }
}

@end
