//
//  AppDelegate.m
//
//  Created by Ajay Thakur on 15/09/17.
//  Copyright © 2016 Rahul Sharma. All rights reserved.


#import "AppDelegate.h"
#import "PGTabBar.h"
#import "UIImageView+WebCache.h"
#import "WebServiceHandler.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "TinderGenericUtility.h"
#import "WebServiceConstants.h"
#import "AFNetworkReachabilityManager.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "WebViewForDetailsVc.h"
#import "Helper.h"
#import "iRate.h"
#import "bitpage-Swift.h"

#define storyBoard [UIStoryboard storyboardWithName:@"Main" bundle:nil]


@import UserNotifications;
@import Firebase;
@import FirebaseInstanceID;
@import FirebaseMessaging;
@class MQTT;
@class MQTTChatManager;


@import GoogleMaps;
@interface AppDelegate ()<SDWebImageManagerDelegate,WebServiceHandlerDelegate >
{
    NSTimer *timer , *adsTimer ,*adsCampaignTimer;
    NSTimer *reconnectTimer;
    NSDictionary *chatResponse;
}
@property MQTTChatManager *mqttChatManager;

@end

@implementation AppDelegate

+ (void)initialize
{
    [iRate sharedInstance].applicationBundleID = mIosBundleId;
    [iRate sharedInstance].appStoreID = 1216265952 ;
    [iRate sharedInstance].onlyPromptIfLatestVersion = NO;
    [iRate sharedInstance].daysUntilPrompt = 2;
    [iRate sharedInstance].usesUntilPrompt = 5;
}

+(AppDelegate*)sharedAppDelegate{
    return  (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{

    NSError* configureError;
    [[GGLContext sharedInstance] configureWithError: &configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    self.mqttChatManager = [MQTTChatManager sharedInstance];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0){
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if( !error ){
                dispatch_async(dispatch_get_main_queue(), ^{
                [[UIApplication sharedApplication] registerForRemoteNotifications];
                    });
            }
        }];
    }
    else {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    [GMSServices provideAPIKey:mGoogleAPIKey];
    [FIRApp configure];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:) name:kFIRInstanceIDTokenRefreshNotification object:nil];
    
    UIViewController* rootController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"TabBarStoryboardID"];
    self.window.rootViewController = rootController;
    
    [self requestForCloundinaryDetails];
    [NSTimer scheduledTimerWithTimeInterval:30.0*60
                                     target:self
                                   selector:@selector(requestForCloundinaryDetails)
                                   userInfo:nil
                                    repeats:NO];
    
    [Fabric with:@[[Crashlytics class]]];
    NSString *token = [[FIRInstanceID instanceID] token];
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:mdeviceToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self handlePushNotificationWithLaunchOption:launchOptions];
    [self setNavigationBar];
    [self addNoInternetConnectionView];
    _internetConnectionErrorView.hidden = YES;
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status){
        if (status == AFNetworkReachabilityStatusNotReachable) {
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isNetworkAvailable"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            _internetConnectionErrorView.hidden = NO;
            [_window bringSubviewToFront:_internetConnectionErrorView];
            
            NSDictionary *dict = @{
                                   @"message":@"NO"
                                   };
            [[NSNotificationCenter defaultCenter] postNotificationName:@"observeNetworkStatus" object:nil userInfo:dict];
        }
        else {
            MQTT *mqttModel = [MQTT sharedInstance];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [mqttModel createConnection];
            });
            _internetConnectionErrorView.hidden = YES;
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isNetworkAvailable"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSDictionary *dict = @{
                                   @"message":@"YES"
                                   };
            [[NSNotificationCenter defaultCenter] postNotificationName:@"observeNetworkStatus" object:nil userInfo:dict];        }
    }];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 1];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
   
    return YES;
}



-(void)requestForCloundinaryDetails{
    [WebServiceHandler getCloudinaryCredintials:@{@"":@""} andDelegate:self];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation] ||
    [[GIDSignIn sharedInstance] handleURL:url sourceApplication:sourceApplication annotation:annotation];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [self.mqttChatManager sendOnlineStatusWithOfflineStatus:YES];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [[FIRMessaging messaging] disconnect];
    [self.mqttChatManager sendOnlineStatusWithOfflineStatus:YES];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [self.mqttChatManager sendOnlineStatusWithOfflineStatus:NO];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [self.mqttChatManager sendOnlineStatusWithOfflineStatus:NO];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"pushToken"];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeUnknown];
    NSLog(@"FIR device token :%@",deviceToken);
    NSString *token = [[FIRInstanceID instanceID] token];
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"pushToken"];
}

-(void)handlePushNotificationWithLaunchOption:(NSDictionary*)launchOptions{
    NSDictionary *receivedDataFromPush = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    NSData *data = [receivedDataFromPush[@"body"] dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsonResponse = [[NSDictionary alloc]init];
    if(data) {
        jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:Nil];
    }
     if (jsonResponse[@"receiverID"]) {
        [self openChatController:jsonResponse];
    }
    else if(receivedDataFromPush) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"openActivity"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    
    NSData *data = [notification.request.content.userInfo[@"body"] dataUsingEncoding:NSUTF8StringEncoding];
    if(data) {
        NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:Nil];
          if (jsonResponse[@"receiverID"]) {
            NSString *userID = [[NSUserDefaults standardUserDefaults]objectForKey:@"currentUserId"] ;
             NSString *productId = [[NSUserDefaults standardUserDefaults]objectForKey:@"productId"] ;
            if ([userID isEqualToString:jsonResponse[@"receiverID"]]
                && [productId isEqualToString:jsonResponse[@"secretID"]] ) {
                
            completionHandler(UNNotificationPresentationOptionSound);
                
            } else {
            completionHandler(UNNotificationPresentationOptionSound | UNNotificationPresentationOptionAlert);
            }
        }
        else {
                completionHandler(UNNotificationPresentationOptionSound | UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionBadge);
            }
        }
    }

-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler {
    [self handleRemoteNotification:[UIApplication sharedApplication] userInfo:response.notification.request.content.userInfo];
}

-(void) handleRemoteNotification:(UIApplication *) application userInfo:(NSDictionary *) remoteNotif {
    if (remoteNotif[@"body"]) {
        NSData *data = [remoteNotif[@"body"] dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:Nil];
         if (jsonResponse[@"receiverID"]) {
            chatResponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:Nil];
            NSDictionary *requestDict = @{
                                          mauthToken :[Helper userToken],
                                          };
            [WebServiceHandler session:requestDict andDelegate:self];

        }
        else {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"openActivityScreen" object:nil];
        }
    }
}




-(void)openChatController:(NSDictionary *)chatData {
    // redirecting it to the next controller with the chat object data.
    // check for empty object.

    UIViewController *tabController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    if ([tabController isKindOfClass:[UITabBarController class]]) {
        UITabBarController *tbControler = (UITabBarController *)tabController;
        dispatch_async(dispatch_get_main_queue(), ^{
                [[[tbControler viewControllers] objectAtIndex:3] popToRootViewControllerAnimated: NO];
                [tbControler setSelectedIndex:3];
        });
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"OfferInitiated" object:nil userInfo:@{@"chatObj":chatData}];
        });
    }
}

- (void)tokenRefreshNotification:(NSNotification *)notification {
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", refreshedToken);
    [[NSUserDefaults standardUserDefaults] setObject:refreshedToken forKey:mdeviceToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self connectToFcm];
}

- (void)connectToFcm {
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Unable to connect to FCM. %@", error);
        } else {
            NSLog(@"Connected to FCM.");
        }
    }];
}

#pragma mark - push sound notification
-(void)playNotificationSound
{
    // play sound
    CFBundleRef mainBundle = CFBundleGetMainBundle();
    CFURLRef soundFileURLRef = CFBundleCopyResourceURL(mainBundle, CFSTR("supcalling"), CFSTR("mp3"), NULL);
    CFRelease(soundFileURLRef);
}

- (void)applicationWillTerminate:(UIApplication *)application {
    
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey: mauthToken];
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey: mUserName];
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey: mMqttId];
    
    [self.mqttChatManager sendOnlineStatusWithOfflineStatus:NO];
}

-(void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    SDImageCache *imageCache = [SDImageCache sharedImageCache];
    [imageCache clearMemory];
    [SDWebImageManager.sharedManager.imageCache clearMemory];
    [SDWebImageManager.sharedManager.imageCache clearDisk];
}

/*-------------------------------------------------------*/
#pragma mark -
#pragma mark - WebServiceDelegate
/*------------------------------------------------------*/

- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {
    if (error) {
        return ;
    }
    //storing the response from server to dictonary.
    NSDictionary *responseDict = (NSDictionary*)response;
    //checking the request type and handling respective response code.
    if (requestType == RequestTypeCloudinaryCredintials ) {
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                // success response.
                [[NSUserDefaults standardUserDefaults] setObject:responseDict forKey:cloudinartyDetails];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
                break;
                
            default:
                break;
        }
    }
    
    if (requestType ==  RequestTypeSession ) {
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                [self openChatController:chatResponse];
                
            }
                break;
                
            default:
                break;
        }
    }
}

- (void)setNavigationBar
{
    if (@available(iOS 11.0, *)) {
        
        [[UIScrollView appearance] setContentInsetAdjustmentBehavior:UIScrollViewContentInsetAdjustmentNever];
        [[UITableView appearance] setContentInsetAdjustmentBehavior: UIScrollViewContentInsetAdjustmentNever] ;
    } else {
        // Fallback on earlier versions
        
    }
    
    [[UINavigationBar appearance] setTranslucent:NO];
    [[UINavigationBar appearance] setShadowImage:nil];
    [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           mNaviColor, NSForegroundColorAttributeName,
                                                           [UIFont fontWithName:HelveticaNeueMedium size:17.0], NSFontAttributeName, nil]];
    
}

#pragma mark - adding internet connection error view

- (void)addNoInternetConnectionView
{
    _internetConnectionErrorView = [[UIView alloc] initWithFrame:CGRectMake(0, 64,[UIScreen mainScreen].bounds.size.width, 20)];
    UILabel *internetConectionErrorLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width, 20)];
    internetConectionErrorLabel.text = @"No internet connection";
    //    [Helper setToLabel:internetConectionErrorLabel Text:@"No internet connection" WithFont:LATO_BOLD FSize:14.0 Color:[UIColor whiteColor]];
    internetConectionErrorLabel.textAlignment = NSTextAlignmentCenter;
    [_internetConnectionErrorView addSubview:internetConectionErrorLabel];
    _internetConnectionErrorView.backgroundColor = [UIColor redColor];
    [self.window addSubview:_internetConnectionErrorView];
}

- (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController {
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
}




-(BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray * _Nullable))restorationHandler
{
        BOOL handleLink = [FIRDynamicLinks.dynamicLinks handleUniversalLink:userActivity.webpageURL completion:^(FIRDynamicLink * _Nullable dynamicLink, NSError * _Nullable error) {
            if  (dynamicLink.url)
            {
                 NSLog(@"Your Dynamic Link parameter: %@",dynamicLink.url);
            } else {
                // Check for errors
            }
            
        }];
        return handleLink ;
}



@end
