//
//  InsightsViewController.h
//  bitpage
//
//  Created by Rahul Sharma on 02/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SimpleBarChart.h"

@interface InsightsViewController : UIViewController
{
    NSArray *_values,*xLabels,*xLabelsMonths,*_valuesMonths;
    
    SimpleBarChart *_chart;
//
    NSArray *_barColors , *yearInsightsArray , *monthInsightsArray;
    NSInteger _currentBarColor;
    BOOL months ;
 
    NSArray *dropDownArray;
    
}

@property(strong, nonatomic)NSArray *basicInsight , *timeInsight , *locationInsight;
@property (strong, nonatomic) IBOutlet UIView *chartView;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightConstraintForChartView;

@property (strong, nonatomic) IBOutlet UIView *sectionHeaderViewForTable;

- (IBAction)chooseTimeFrameButtonAction:(id)sender;

@property (strong,nonatomic) NSString *postId ;
@property (strong, nonatomic) IBOutlet UILabel *postedTimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *postedDateLabel;

@property (strong, nonatomic) IBOutlet UILabel *uniqueClicksCount;
@property (strong, nonatomic) IBOutlet UILabel *reviewsCount;
@property (strong, nonatomic) IBOutlet UILabel *totalClicksCount;
@property (strong, nonatomic) IBOutlet UILabel *likesCount;

@property (strong, nonatomic) IBOutlet UILabel *labelForTimeFrame;
@property (strong, nonatomic) IBOutlet UIButton *dropDownButton;
@property (strong, nonatomic) IBOutlet UITableView *dropDownTabbleView;
@property (strong, nonatomic) IBOutlet UIView *dropDowndividerView;

@property (strong, nonatomic) IBOutlet UIView *dropDownFullView;
@property (strong, nonatomic) IBOutlet UITableView *countryTableView;
@property (strong, nonatomic) IBOutlet UIView *viewForCountryTableView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *countryTableViewHeightConstraint;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UIImageView *dropDownImageOutlet;
//postProduct__dropDown
@property (strong, nonatomic) IBOutlet UIButton *thisWeekBtnOutlet;

@property (strong, nonatomic) IBOutlet UIButton *thisMonthBtnOutlet;
@property (strong, nonatomic) IBOutlet UIButton *thisYearBtnOutlet;
@property (strong, nonatomic) IBOutlet UIView *thisWeekUnderView;
@property (strong, nonatomic) IBOutlet UIView *thisMonthUnderView;
@property (strong, nonatomic) IBOutlet UIView *thisYearUnderView;
- (IBAction)thisWeekBtnAction:(UIButton *)sender;
- (IBAction)thisMonthBtnAction:(UIButton *)sender;
- (IBAction)thisYearBtnAction:(UIButton *)sender;

@end
