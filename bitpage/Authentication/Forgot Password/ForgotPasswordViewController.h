
//
//  Created by Rahul_Sharma on 04/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *sendLoginLinkButtonOutlet;
- (IBAction)sendLoginLinkButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
- (IBAction)tapToDismissKeyboard:(id)sender;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
- (IBAction)emailTextfieldValueIsChanged:(id)sender;
- (IBAction)resendCodeAction:(UIButton *)sender;

@end
