//
//  LandingPageController.m
//  Created by Rahul Sharma on 12/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "LandingPageViewController.h"
#import "LoginSignUpViewController.h"
#import "FBLoginHandler.h"
#import "WebViewForDetailsVc.h"
#import "UserDetails.h"
#import "bitpage-Swift.h"
#import "ForgotPasswordViewController.h"

@class MQTT;
@interface LandingPageViewController ()<FBLoginHandlerDelegate, WebServiceHandlerDelegate,GIDSignInDelegate,GetCurrentLocationDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate>
{
    int loginType;// Login = 0 , Google Sign in = 2, FacebookLogin = 1
    UITextField *activeTextField;
}
@end

@implementation LandingPageViewController

/*-----------------------------------*/
#pragma mark
#pragma mark -  ViewController LifeCycle -
/*-----------------------------------*/

- (void)viewDidLoad {
    [super viewDidLoad];
    getLocation = [GetCurrentLocation sharedInstance];
    [getLocation getLocation];
    getLocation.delegate = self;
     self.signInButtonOutlet.backgroundColor = mLoginButtonDisableBackgroundColor;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShown:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];

    self.navigationController.navigationBar.hidden = YES;
    
}


-(BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*-----------------------------------*/
#pragma mark
#pragma mark - Location Delegate -
/*-----------------------------------*/


/**
 Location Handler Model Delegate Methods.

 @param latitude  current lattitude fetched bu location handler.
 @param longitude  current longitude fetched bu location handler.
 */
- (void)updatedLocation:(double)latitude and:(double)longitude
{
    self.currentLat = latitude;
    self.currentLong = longitude;
    
}


/**
 This method will give the updated address from location handler Model.

 @param currentAddress location address.
 */
- (void)updatedAddress:(NSString *)currentAddress
{
    
    
}



/*-----------------------------------*/
#pragma mark
#pragma mark - textfield Delegates for LogIn View.
/*-----------------------------------*/

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeTextField = textField ;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {

    if (textField == self.usernameTextField) {
        [self.loginPasswordTextfield becomeFirstResponder];
    }else if(textField == self.loginPasswordTextfield)
        [self.view endEditing:YES];
    return YES;
}

//- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
//    [self.view endEditing:YES];
//    return YES;
//}
/**
 This method will check Log in Button state.
 @return Yes for enable and No for disable.
 */
- (BOOL)checkForMandatoryField
{
    if (self.loginPasswordTextfield.text.length < 2 || self.usernameTextField.text.length < 2){
        self.signInButtonOutlet.enabled = NO;
        _signInButtonOutlet.backgroundColor =  mLoginButtonDisableBackgroundColor;
        self.signInButtonOutlet.titleLabel.textColor = mLoginTextColor;
        return NO;
    }
    else {
        self.signInButtonOutlet.enabled = YES;
        _signInButtonOutlet.backgroundColor = mLoginButtonEnableBackgroundColor;
        self.signInButtonOutlet.titleLabel.textColor = [UIColor whiteColor];
        return YES;
    }
}

/**
 This method will recognize the textfield to make that first Responder
 @param tag int value
 */
-(void)textFieldBecomeFirstResponder :(NSInteger ) tag
{
    switch (tag) {
        case 0:
            [self.usernameTextField becomeFirstResponder];
            break;
        case 1:
            [self.loginPasswordTextfield resignFirstResponder];
            break;

    }
}

/*-----------------------------------*/
#pragma mark
#pragma mark - Button Actions -
/*-----------------------------------*/

#pragma mark - LogIn Button Action -

- (IBAction)loginButtonAction:(id)sender {
    loginType = 0;
    [self LoginButtonSelected];
}
/**
Method will request web service for login.
*/
-(void)LoginButtonSelected{
    //animating activity view(progress bar).
    [self.activityViewOutlet startAnimating];
     [self.signInButtonOutlet.titleLabel setHidden:YES];
    // login api requesting.
    NSDictionary *requestDict = @{
                                  mLoginType   : mUserNormalLogin,
                                  mUserName    : self.usernameTextField.text,
                                  mPswd :self.loginPasswordTextfield.text ,
                                  mlatitude : [NSString stringWithFormat:@"%lf",self.currentLat],
                                  mlongitude : [NSString stringWithFormat:@"%lf",self.currentLong],
                                  mCity : flStrForObj(getLocation.currentCity),
                                  mCountryShortName : flStrForObj(getLocation.countryShortCode),
                                  mpushToken   :flStrForObj([Helper deviceToken]),
                                  };
    [WebServiceHandler logId:requestDict andDelegate:self];
}


#pragma mark - SignUp Button Action -

- (IBAction)signupButtonAction:(id)sender {
    
    LoginSignUpViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:mLoginScreenID];
    loginVC.signupFlag = YES;
    loginVC.SignUpType = mUserNormalLogin;
    [self.navigationController pushViewController:loginVC animated:YES];
}

#pragma mark
#pragma mark - tapGesture
/*--------------------------------------*/
- (IBAction)tapGestureAction:(id)sender {
    
    [self.view endEditing:YES];
}

#pragma mark - Terms&Condition Button Action -


/**
 This method will redirect user to terms & conditions Page.

 @param sender terms&conditions onject.
 */
- (IBAction)termsButtonACtion:(id)sender {
    
    WebViewForDetailsVc *newView = [self.storyboard instantiateViewControllerWithIdentifier:mDetailWebViewStoryBoardId];
    newView.showTermsAndPolicy = NO;
    [self.navigationController pushViewController:newView animated:YES];
}

#pragma mark - Privacy Button Action -


/**
 This method will redirect user to privacy page.

 @param sender privacy button object.
 */
- (IBAction)privacyButtonAction:(id)sender {
    WebViewForDetailsVc *newView = [self.storyboard instantiateViewControllerWithIdentifier:mDetailWebViewStoryBoardId];
    newView.showTermsAndPolicy = YES;
    [self.navigationController pushViewController:newView animated:YES];
    
}

- (IBAction)dismissKeyboard:(id)sender {
//        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
//            [self.usernameTextField resignFirstResponder];
//            [self.loginPasswordTextfield resignFirstResponder];
//            CGRect frameOfView = self.view.frame;
//            frameOfView.origin.y = 0;
//            frameOfView.origin.x=0;
//            self.view.frame = frameOfView;
//        } completion:^(BOOL finished) {
//    }];
    [self.view endEditing:YES];
    [self view_animation:0];
}

#pragma mark - remember Me Button Action -


- (IBAction)rememberMeButtonAction:(id)sender {
    self.rememberMeButtonOutlet.selected = !self.rememberMeButtonOutlet.selected;
}
- (IBAction)textFieldDidChange:(id)sender {
    [self checkForMandatoryField];
}
-(void)view_animation:(CGFloat)yaxis{
    [UIView beginAnimations:nil context:NULL];
    self.view.frame = CGRectMake(0,yaxis,self.view.frame.size.width,self.view.frame.size.height);
    [UIView commitAnimations];
}

/**
 Get help for signin if password is forgotten.
 
  forgotPasswordButtonAction.
 */
- (IBAction)forgotPasswordButtonAction:(id)sender {
        ForgotPasswordViewController *forgotVC = [self.storyboard instantiateViewControllerWithIdentifier:mForgotPasswordStoryboardID];
    [self.navigationController pushViewController:forgotVC animated:YES];
}

- (IBAction)signUpForFreeAccountButtinAction:(id)sender {
    LoginSignUpViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:mLoginScreenID];
    loginVC.signupFlag = YES;
    loginVC.SignUpType = mUserNormalLogin;
    [self.navigationController pushViewController:loginVC animated:YES];
}

#pragma mark - Close Button Action -


/**
 Dismiss Landing (Login/Signup) permission screen.

 @param sender close button object.
 */
- (IBAction)closeButtonAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Facebook Login Button -


/**
 Continue with Facebook.
 This method will redirect to facebook login screen and invoke facebook Login handler.
 If user is already registered with facebook account, login directly else bring for signup.

 @param sender continue With Facebook button.
 */
- (IBAction)continueFacbookButtonAction:(id)sender {
    loginType = 1;
    FBLoginHandler *handler = [FBLoginHandler sharedInstance];
    [handler loginWithFacebook:self];
    [handler setDelegate:self];
    
}

#pragma mark - Google Login Button -

/**
 Continue with Google.
 This method will redirect to Google login screen and invoke Google Login handler.
 If user is already registered with Google account, login directly else bring for signup.
 
 @param sender continue With Google button.
 */
- (IBAction)googleLoginAction:(id)sender {
    loginType = 2;
    GIDSignIn *signin = [GIDSignIn sharedInstance];
    [signin signOut];
    signin.shouldFetchBasicProfile = true;
    signin.delegate = self;
    signin.uiDelegate = self;
    [signin signIn];
}
/*--------------------------------------------*/
#pragma mark
#pragma mark - facebook handler
/*--------------------------------------------*/

/**
 Facebook handler get call on success of facebook service.
 
 @param userInfo user information in dictionary.
 */
- (void)didFacebookUserLoginWithDetails:(NSDictionary*)userInfo {
    
    self.fbloggedUserAccessToken = flStrForObj([FBSDKAccessToken currentAccessToken].tokenString);
    
    
    NSDictionary *requestDict = @{mLoginType :mUserFacebookLogin,
                                  mfaceBookId :flStrForObj(userInfo[@"id"]),
                                  mEmail :flStrForObj(userInfo[@"email"]),
                                  mpushToken   :flStrForObj([Helper deviceToken]),
                                  mlatitude : [NSString stringWithFormat:@"%lf",self.currentLat],
                                  mlongitude : [NSString stringWithFormat:@"%lf",self.currentLong],
                                  mCity : flStrForObj(getLocation.currentCity),
                                  mCountryShortName : flStrForObj(getLocation.countryShortCode),
                                  }
    ;
    [WebServiceHandler logId:requestDict andDelegate:self];
    
    self.fbLoginDetails = userInfo;
    self.faceBookUniqueIdOfUser = userInfo[@"id"];
    self.faceBookUserEmailId  =  flStrForObj( userInfo[@"email"]);
    self.fullNameForFb = flStrForObj( userInfo[@"name"]);
    if (!(self.faceBookUserEmailId.length >1)) {
        NSString *UniqueMailId = [self.faceBookUniqueIdOfUser stringByAppendingString:@"@facebook.com"];
        self.faceBookUserEmailId = UniqueMailId;
    }
    NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", self.faceBookUniqueIdOfUser]];
    self.profilepicurlFb = flStrForObj(pictureURL.absoluteString);
    
}

/**
 If facebook handler get unsuccessful rsponse.
 
 @param error Description.
 */
- (void)didFailWithError:(NSError *)error {
    [Helper showAlertWithTitle:NSLocalizedString(alertError, alertError) Message:NSLocalizedString(mFacebookLoginErrorMessage, mFacebookLoginErrorMessage) viewController:self];
}


/*-----------------------------*/
#pragma mark -
#pragma mark - WebServiceDelegate
/*-----------------------------*/

/**
 Webservice delegate method will get call on success of service call.
 
 @param requestType requestType sent.
 @param response    response comes from webservice.
 @param error       error if any.
 */
- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {
    [self.activityViewOutlet stopAnimating];
    if (error) {
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
            CGRect frameOfView = self.view.frame;
            frameOfView.origin.y = 0;
            frameOfView.origin.x=0;
            self.view.frame = frameOfView;
        } completion:^(BOOL finished) {
            //code for completion
            [Helper showAlertWithTitle:NSLocalizedString(alertError, alertError) Message:NSLocalizedString(mLoginFailedMessage, mLoginFailedMessage) viewController:self];
        }];
        return;
    }
    
    if (requestType == RequestTypeLogin ) {
        UserDetails *user = response ;
        switch (user.code) {
            case 200: {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"recent_login"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [Helper storeUserLoginDetails:user];
                NSDictionary *param = [CommonMethods updateDeviceDetailsForAdmin];
                [WebServiceHandler logUserDevice:param andDelegate:self];
                //Check Campaign
                NSDictionary *requestDic = @{mauthToken : [Helper userToken] };
                [WebServiceHandler runCampaign:requestDic andDelegate:self];
                
                [self.parentViewController dismissViewControllerAnimated:YES completion:nil];
                
                // MQTT Connection Created
                MQTT *mqttModel = [MQTT sharedInstance];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [mqttModel createConnection];
                });
                
            }
                break;
            case 307: {
                LoginSignUpViewController *signupVC=[self.storyboard instantiateViewControllerWithIdentifier:mLoginScreenID];
                signupVC.signupFlag =  YES;
                signupVC.signUpWithFacebook = YES;
                signupVC.SignUpType = mUserGooglePlusSignup;
                [self.navigationController pushViewController:signupVC animated:YES];
            }
                break;
            case 204: {
                LoginSignUpViewController *signupVC=[self.storyboard instantiateViewControllerWithIdentifier:mLoginScreenID];
                signupVC.signupFlag =  YES;
                if(loginType == 2)
                {
                signupVC.signUpWithGooglePlus = YES;
                signupVC.googlePlusName = self.googlePlusName;
                signupVC.googlePlusProfileUrl = self.googlePlusProfileUrl;
                signupVC.googlePlusId = self.googlePlusId;
                signupVC.googlePlusAccessToken = self.googlePlusUserAccessToken;
                signupVC.googlePlusEmail = self.googlePlusEmailId ;
                signupVC.SignUpType = mUserGooglePlusSignup;
                }
                else if(loginType == 0)
                {
                    [Helper showAlertWithTitle:NSLocalizedString(muserNotFoundAlertTitle, muserNotFoundAlertTitle) Message:NSLocalizedString(userNotFound, userNotFound) viewController:self];
                }
                else
                {
                signupVC.signUpWithFacebook = YES;
                signupVC.fbResponseDetails =  self.fbLoginDetails;
                signupVC.fbloggedUserAccessToken = self.fbloggedUserAccessToken;
                signupVC.SignUpType = mUserFacebookSignup;
                }
                [self.navigationController pushViewController:signupVC animated:YES];
            }
                break;
            case 401: {
                [Helper showAlertWithTitle:NSLocalizedString(mincorrectPasswordAlertTitle, mincorrectPasswordAlertTitle) Message:NSLocalizedString(incorrectPasswordMessage, incorrectPasswordMessage) viewController:self];
            }
                break;
            default:
                [Helper showAlertWithTitle:NSLocalizedString(alertTitle, alertTitle) Message:user.message viewController:self];
                break;
        }
     }
    
    }


/*--------------------------------------------*/
#pragma mark
#pragma mark - Google Login  handler -
/*--------------------------------------------*/

/**
 Google Login Handler delegate methods.
 

 @param signIn Google SignedIn object.
 @param user   signed User Details.
 @param error  error if signIn was not successful.
 */
- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    if (error == nil) {
        
        self.googlePlusUserAccessToken = user.authentication.accessToken;
        self.googlePlusId = user.userID ;
        self.googlePlusEmailId  = user.profile.email ;
        self.googlePlusName = user.profile.name;
        self.googlePlusProfileUrl = [user.profile imageURLWithDimension:300].absoluteString;
        
        NSDictionary *requestDict = @{mLoginType  :mUserGooglePlusLogin ,
                                      mGooglePlusId :flStrForObj(user.userID),
                                      mEmail :flStrForObj(user.profile.email),
                                      mpushToken   :flStrForObj([Helper deviceToken]),
                                      mlatitude : [NSString stringWithFormat:@"%lf",self.currentLat],
                                      mlongitude : [NSString stringWithFormat:@"%lf",self.currentLong],
                                      mCity : flStrForObj(getLocation.currentCity),
                                      mCountryShortName : flStrForObj(getLocation.countryShortCode),
                                      
                                      };
        [WebServiceHandler logId:requestDict andDelegate:self];
        
        
    } else {
        NSLog(@"%@", error.localizedDescription);
    }
}


/**
 SignIn cancelled by user.

 @param signIn Google SignedIn object.
 @param user   user Details fetched before cancelling Sign In.
 @param error  error if any.
 */
- (void)signIn:(GIDSignIn *)signIn didDisconnectWithUser:(GIDGoogleUser *)user withError:(NSError *)error {

    // Perform any operations on signed in user here.
    if (error == nil) {
        
    } else {
        NSLog(@"%@", error.localizedDescription);
    }
}

- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
    
    if(error){
         [Helper showAlertWithTitle:NSLocalizedString(alertError, alertError) Message:NSLocalizedString(mGoogleLoginErrorMessage, mGoogleLoginErrorMessage) viewController:self];
    }
}


/**
 This method will Present the google LogIn Controller over present ViewController.

 @param viewController viewController refrence object.
 */
- (void)presentSignInViewController:(UIViewController *)viewController {
    [[self navigationController] presentViewController:viewController animated:YES completion:nil];
}


/**
 Dismiss ViewController on Successfull fetching userDetails or on cancelling.

 @param signIn         signInDescription Object.
 @param viewController refrence object of viewController.
 */
- (void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}





#pragma mark
#pragma mark - Move View

/**
 This method will move view by evaluating keyboard height.
 
 @param n post by Notificationcentre.
 */

- (void)keyboardWillShown:(NSNotification *)n
{
    NSDictionary* info = [n userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, activeTextField.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, activeTextField.frame.origin.y-kbSize.height);
        [self.scrollView setContentOffset:scrollPoint animated:YES];
    }
    
}

- (void)keyboardWillHide:(NSNotification *)n
{
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
}


@end
