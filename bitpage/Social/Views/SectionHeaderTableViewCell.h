//
//  SectionHeaderTableViewCell.h
//  bitpage
//
//  Created by Rahul Sharma on 28/12/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SectionHeaderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet AlignmentOfLabel *userNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *timeStampLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userProfileImageOutlet;

@property (weak, nonatomic) IBOutlet UIButton *headerProfileButtonOulet;
@property (strong, nonatomic) IBOutlet UIButton *reportButtonOutlet;

- (IBAction)reportButtonAction:(UIButton *)sender;

-(void)setSectionHeader :(ProductDetails *)product;
@end
