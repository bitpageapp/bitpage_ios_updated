
//  ProductImageTableViewCell.h
//  Created by Ajay Thakur on 16/09/17.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ProductImageTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageViewOutlet;


-(void)setProductImage :(ProductDetails *)product;


@end



