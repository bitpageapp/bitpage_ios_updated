//
//  AddressTableViewCell.h
//  Vendu
//
//  Created by Rahul Sharma on 19/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostListingsViewController.h"

@interface AddressTableViewCell : UITableViewCell

@property (nonatomic,strong) Listings *listing ;
@property (nonatomic)BOOL isEditPost ;
@property (nonatomic, strong)PostListingsViewController *refrenceVC ;
@property (weak, nonatomic) IBOutlet AlignmentOfLabel *addressLabel;
@property (weak, nonatomic) IBOutlet UIButton *removeAddressButton;

-(void)setAddress ;

- (IBAction)removeAddressAction:(id)sender;

- (IBAction)addAddressButtonAction:(id)sender;


@end
