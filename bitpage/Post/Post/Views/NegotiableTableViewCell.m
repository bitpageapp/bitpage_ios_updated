//
//  NegotiableTableViewCell.m
//  Vendu
//
//  Created by Rahul Sharma on 31/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "NegotiableTableViewCell.h"

@implementation NegotiableTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setStateForNegtiableSwitch :(BOOL)state
{
    self.negotiableSwitch.selected = state;
    self.listing.negotiable = state;
}

@end
