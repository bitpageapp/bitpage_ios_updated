//
//  Listings.h
//  Vendu
//
//  Created by Rahul Sharma on 20/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Listings : NSObject

@property(nonatomic,copy) NSString *titleOfPost;
@property(nonatomic,copy) NSString *descriptionForPost;
@property(nonatomic,copy) NSString *category;
@property(nonatomic,copy) NSString *condition;
@property(nonatomic,copy) NSString *currency;
@property(nonatomic,copy) NSString *price;
@property(nonatomic)BOOL  negotiable;
@property(nonatomic,copy) NSString *address;
@property(nonatomic) double lattitude ;
@property(nonatomic) double longitude ;
@property(nonatomic)BOOL  isFacebookShare;
@property(nonatomic)BOOL  isTwitterShare;
@property(nonatomic)BOOL  isInstagramShare;
@property(nonatomic,copy) NSArray * arrayOfImagePaths;
@property(nonatomic,copy) NSString *cityName;
@property(nonatomic,copy) NSString *countrySName;
@property(nonatomic,copy) NSString *postId;
@property(nonatomic,weak) UIViewController *refrenceVC;

-(BOOL)isListingDone;

@end
