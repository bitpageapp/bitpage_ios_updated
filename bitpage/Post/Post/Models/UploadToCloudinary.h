//
//  UploadToCloudinary.h

//
//  Created by Rahul Sharma on 14/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Cloudinary.h"

@protocol CloudinaryModelDelegate <NSObject>
-(void)getDictionaryFromCloudinaryModelClass:(NSDictionary *)requestDic;
-(void)cloudinaryError:(NSString *)errorResult andCheck:(BOOL)isError ;


@end
@interface UploadToCloudinary : NSObject <CLUploaderDelegate>

@property(nonatomic,strong)NSString *mainImageUrl;
@property(nonatomic,strong)NSString *ImageUrl1;
@property(nonatomic,strong)NSString *ImageUrl2;
@property(nonatomic,strong)NSString *ImageUrl3;
@property(nonatomic,strong)NSString *ImageUrl4;
@property(nonatomic,strong)NSString *mainThumbnailUrl;

@property(nonatomic,strong)NSString *cloudinaryPublicId ;
@property(nonatomic,strong)NSString *cloudinaryPublicId1 ;
@property(nonatomic,strong)NSString *cloudinaryPublicId2 ;
@property(nonatomic,strong)NSString *cloudinaryPublicId3 ;
@property(nonatomic,strong)NSString *cloudinaryPublicId4 ;

@property(nonatomic,strong)NSArray *arrayOfimagePaths ;
@property(nonatomic,strong)NSDictionary *cloundinaryCreditinals;
@property(nonatomic,strong)UploadToCloudinary *cloudObj;
@property(nonatomic,weak)id<CloudinaryModelDelegate>delegate;
+(instancetype)sharedInstance;
-(id)initWithArrayOfImagePaths :(NSArray *)arrayOfPaths ;
-(void)uploadingImageToCloudinaryWithArrayOfPaths;
@end
