//
//  cellForListOfOptions.m

//
//  Created by Rahul Sharma on 14/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "cellForListOfOptions.h"

@implementation cellForListOfOptions

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)updateCell :(NSArray *)dataArray withKey:(NSString *)key IndexPath :(NSIndexPath *)indexpath andPreviousSelection:(NSString *)previousSelection
{
    
    if ([key isEqualToString:@"condition"]){
        self.labelForList.text = dataArray[indexpath.row];
    }
    else{
        self.labelForList.text = dataArray[indexpath.row][@"name"];
        [self.iconImage sd_setImageWithURL:[NSURL URLWithString:flStrForObj(dataArray[indexpath.row][@"activeimage"])] placeholderImage:[UIImage imageNamed:@"itemProdDefault"]];
    }
    
    self.labelForList.text = [self.labelForList.text capitalizedString];
    
    if([previousSelection isEqualToString:self.labelForList.text])
        self.imageViewForSelection.hidden = NO;
    else
        self.imageViewForSelection.hidden = YES;
}

@end
