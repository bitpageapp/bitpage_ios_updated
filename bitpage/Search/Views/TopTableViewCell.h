//
//  TopTableViewCell.h

//
//  Created by Rahul Sharma on 7/25/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *userNameOutlet;

@property (weak, nonatomic) IBOutlet UILabel *fullNameOutlet;

@property (weak, nonatomic) IBOutlet UIImageView *profileImageViewOutlet;
@property (strong, nonatomic) IBOutlet UILabel *priceLabel;

@property (strong, nonatomic) IBOutlet UIImageView *currencyImgView;
@property (strong, nonatomic) IBOutlet UILabel *currLabel;
@property (strong, nonatomic) IBOutlet UILabel *categoryLabel;

-(void)setValuesInCellWithUsername:(NSString *)username fullName :(NSString *)fullname ProfileImage:(NSString *)profileImageUrl price:(NSString *)price andcurrImg:(NSString *)currImg curr:(NSString *)curr category:(NSString *)cat;
@end
