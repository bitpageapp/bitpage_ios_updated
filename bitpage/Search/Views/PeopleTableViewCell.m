//
//  PeopleTableViewCell.m

//
//  Created by Rahul Sharma on 7/25/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "PeopleTableViewCell.h"

@implementation PeopleTableViewCell
{
    BOOL isFollowing;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self layoutIfNeeded];
    isFollowing = NO;
    self.profileImageViewOutlet.layer.cornerRadius = self.profileImageViewOutlet.frame.size.height/2;
    self.profileImageViewOutlet.clipsToBounds = YES;
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setValuesInCellWithUsername:(NSString *)username fullName :(NSString *)fullname andProfileImage:(NSString *)profileImageUrl
{
    self.userNameLabel.text= username;
    self.fullnameLabel.text = [NSString stringWithFormat:@"@%@",fullname];
    [self.profileImageViewOutlet sd_setImageWithURL:[NSURL URLWithString:profileImageUrl] placeholderImage:[UIImage imageNamed:@"defaultpp"]];
}
-(void)sendNewFollowStatusThroughNotification:(NSString *)username andNewStatus:(NSString *)newFollowStatus {
    NSDictionary *newFollowDict = @{@"newFollowStatus"     :flStrForObj(newFollowStatus),
                                    mUserName            :flStrForObj(username),
                                    };
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updatedFollowStatus" object:[NSDictionary dictionaryWithObject:newFollowDict forKey:@"newUpdatedFollowData"]];
}

-(void)unfollowAction:(id)sender {
    UIButton *selectedButton = (UIButton *)sender;
    [selectedButton makeButtonAsFollow];
    [self sendNewFollowStatusThroughNotification:self.prod.postedByUserName andNewStatus:@"2"];
    //passing parameters.
    NSDictionary *requestDict = @{muserNameToUnFollow:flStrForObj(self.prod.membername),
                                  mauthToken            :flStrForObj([Helper userToken]),
                                  };
    //requesting the service and passing parametrs.
    [WebServiceHandler unFollow:requestDict andDelegate:self];
}
- (IBAction)followbtnAction:(UIButton *)sender {
    UIButton *selectedButton = (UIButton *)sender;
    //actions for when the account is public.
    if ([selectedButton.titleLabel.text containsString:NSLocalizedString(followingButtonTitle, followingButtonTitle)]) {
        [self unfollowAction:sender];
    }
    else {
        [selectedButton makeButtonAsFollowing];
        [self sendNewFollowStatusThroughNotification:self.prod.username andNewStatus:@"1"];
        //passing parameters.
        
        NSDictionary *requestDict = @{muserNameTofollow     :flStrForObj(self.prod.membername),
                                      mauthToken            :flStrForObj([Helper userToken]),
                                      };
        //requesting the service and passing parametrs.
        [WebServiceHandler follow:requestDict andDelegate:self];
    }
}
@end
