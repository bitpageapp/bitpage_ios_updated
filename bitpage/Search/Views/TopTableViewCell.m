//
//  TopTableViewCell.m

//
//  Created by Rahul Sharma on 7/25/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "TopTableViewCell.h"

@implementation TopTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self layoutIfNeeded];
//    self.profileImageViewOutlet.layer.cornerRadius =self.profileImageViewOutlet.frame.size.height/2;
    self.profileImageViewOutlet.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setValuesInCellWithUsername:(NSString *)username fullName :(NSString *)fullname ProfileImage:(NSString *)profileImageUrl price:(NSString *)price andcurrImg:(NSString *)currImg curr:(NSString *)curr category:(NSString *)cat{
    self.userNameOutlet.text = username;
    self.fullNameOutlet.text = fullname;
    self.priceLabel.text = price;
    self.currLabel .text = curr;
    self.categoryLabel.text = cat;
    [self.currencyImgView setImage:[UIImage imageNamed:curr]];
    [ self.profileImageViewOutlet sd_setImageWithURL:[NSURL URLWithString:profileImageUrl] placeholderImage:[UIImage imageNamed:@"defaultpp"]];
}
@end
