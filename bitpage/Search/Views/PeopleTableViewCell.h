//
//  PeopleTableViewCell.h

//
//  Created by Rahul Sharma on 7/25/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductDetails.h"


@interface PeopleTableViewCell : UITableViewCell<WebServiceHandlerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *fullnameLabel;
- (IBAction)followbtnAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet ProductDetails *prod;

@property (weak, nonatomic) IBOutlet UIImageView *profileImageViewOutlet;
@property (strong, nonatomic) IBOutlet UIButton *followBtnOutlet;

-(void)setValuesInCellWithUsername:(NSString *)username fullName :(NSString *)fullname andProfileImage:(NSString *)profileImageUrl;
@end
