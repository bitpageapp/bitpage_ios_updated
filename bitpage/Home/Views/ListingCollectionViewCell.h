//
//  ListingCollectionViewCell.h
//  Created by Rahul Sharma on 4/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "CurrencySelectVC.h"


@interface ListingCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIView *featuredView;
@property (weak, nonatomic) IBOutlet UIImageView *postedImageOutlet;
@property (weak, nonatomic) IBOutlet UIView *priceNameView;
@property (weak, nonatomic) IBOutlet UILabel *productName;
@property (strong, nonatomic) CurrencySelectVC *refVC;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (strong, nonatomic) IBOutlet UILabel *subCatLabel;

-(void)setProducts:(ProductDetails *)product;
@property (strong, nonatomic) IBOutlet UIImageView *currencySymbolImage;
@property (strong, nonatomic) IBOutlet UIImageView *homeLikeImgView;

@end
