//
//  CellForReset.h
//  bitpage
//
//  Created by 3Embed Software Technologies Pvt LTd on 19/04/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CellForReset : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *resetButtonOutlet;

@end

NS_ASSUME_NONNULL_END
