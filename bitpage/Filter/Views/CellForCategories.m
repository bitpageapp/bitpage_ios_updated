//
//  CellForCategories.m

//
//  Created by Rahul Sharma on 02/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "CellForCategories.h"
#import "CollectionCategoriesCell.h"


#define mCategoryNameResponseKey       @"name"
#define mCategoryActiveImageKey        @"activeimage"
#define mcategoryDeactiveImageKey      @"deactiveimage"



@implementation CellForCategories
{
    NSArray *arrayOfImages;
    NSMutableString *selectedCategory;
    NSURL *imageUrl;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    //[[NSArray alloc]initWithObjects:@"free stuff",@"electronics",@"cars and motors",@"sports, leisure and games",@"home and garden",@"movies, books and music",@"fashion and accessories",@"baby and child",@"other",nil];
    
    _collectionView.delegate=self;
    _collectionView.dataSource=self;
    
}


/*--------------------------------------*/
#pragma mark -
#pragma mark - CollectionView DataSource Method
/*--------------------------------------*/

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
//    if(self.arrayOfCategoryList.count % 2)
//    { return self.arrayOfCategoryList.count + 1 ;}
//
    return self.arrayOfCategoryList.count ;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CollectionCategoriesCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"cellCategory" forIndexPath:indexPath];
    
    [cell setValuesWithArray:self.arrayOfCategoryList indexPath:indexPath andIconsArray:nil];
    
    
    if( indexPath.row != _arrayOfCategoryList.count && [_arrayOfDefaults containsObject:self.arrayOfCategoryList [indexPath.row][@"name"]])
    {
        cell.labelForCategory.textColor = [UIColor whiteColor] ;
        cell.viewForCat.backgroundColor = mBaseColor;
        [cell.imageView sd_setImageWithURL:[NSURL URLWithString:flStrForObj(self.arrayOfCategoryList[indexPath.row][mCategoryActiveImageKey])] placeholderImage:[UIImage imageNamed:@""]];
    }
    else if (indexPath.row != self.arrayOfCategoryList.count) {
        cell.labelForCategory.textColor = [UIColor colorWithRed:72/255.0f green:72/255.0f blue:72/255.0f alpha:1.0f];
        cell.viewForCat.backgroundColor = [UIColor colorWithRed:72/255.0f green:72/255.0f blue:72/255.0f alpha:0.2f];

        [cell.imageView sd_setImageWithURL:[NSURL URLWithString:flStrForObj(self.arrayOfCategoryList[indexPath.row][mcategoryDeactiveImageKey])] placeholderImage:[UIImage imageNamed:@""]];
    }
    return cell;
}

/*-------------------------------------------*/
 #pragma mark -
 #pragma mark - CollectionView Delegate Method
 /*-------------------------------------------*/


 -(CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    return CGSizeMake((_collectionView.frame.size.width/3)-0.5,100);
}

-(void)collectionView:(UICollectionView *)cv didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{   CollectionCategoriesCell *cell = (CollectionCategoriesCell*)[cv cellForItemAtIndexPath:indexPath] ;
    
    if( indexPath.row != _arrayOfCategoryList.count && ![_arrayOfDefaults containsObject:self.arrayOfCategoryList [indexPath.row][@"name"]])
    {
        cell.labelForCategory.textColor = [UIColor whiteColor] ;
        cell.viewForCat.backgroundColor = mBaseColor ;
        
       [cell.imageView sd_setImageWithURL:[NSURL URLWithString:flStrForObj(self.arrayOfCategoryList[indexPath.row][mCategoryActiveImageKey])] placeholderImage:cell.imageView.image];
    
        selectedCategory = self.arrayOfCategoryList [indexPath.row][mCategoryNameResponseKey];
        imageUrl = self.arrayOfCategoryList[indexPath.row][mCategoryActiveImageKey];
        
        if(self.callBackFilter)
        {  self.callBackFilter(selectedCategory, imageUrl); }
        
    }
    else if (indexPath.row != self.arrayOfCategoryList.count) {
            cell.labelForCategory.textColor = [UIColor colorWithRed:72/255.0f green:72/255.0f blue:72/255.0f alpha:1.0f];
        cell.viewForCat.backgroundColor = [UIColor colorWithRed:72/255.0f green:72/255.0f blue:72/255.0f alpha:0.2f];

        [cell.imageView sd_setImageWithURL:[NSURL URLWithString:flStrForObj(self.arrayOfCategoryList[indexPath.row][mcategoryDeactiveImageKey])] placeholderImage: cell.imageView.image];
        
        selectedCategory = self.arrayOfCategoryList [indexPath.row][mCategoryNameResponseKey];
        imageUrl = self.arrayOfCategoryList[indexPath.row][mCategoryActiveImageKey];

        if(self.callBackFilter)
            self.callBackFilter(selectedCategory, imageUrl);
    }
}


@end
