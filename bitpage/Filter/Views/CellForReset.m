//
//  CellForReset.m
//  bitpage
//
//  Created by 3Embed Software Technologies Pvt LTd on 19/04/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

#import "CellForReset.h"

@implementation CellForReset

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
