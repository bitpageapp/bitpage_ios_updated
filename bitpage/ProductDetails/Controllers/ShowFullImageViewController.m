//
//  ShowFullImageViewController.m

//
//  Created by Rahul Sharma on 21/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "ShowFullImageViewController.h"
#import "FontDetailsClass.h"

@interface ShowFullImageViewController () <UIScrollViewDelegate,UIGestureRecognizerDelegate>
{
    CGFloat imagMinScale;
    UIButton *navRight;
    CGFloat beginX;
    CGFloat beginY;
    CGFloat previousRotation;
    UISwipeGestureRecognizer *swipeDown;
    NSLayoutConstraint *leftViewHorizondalRightPadding;
    NSLayoutConstraint *rightViewHorizondalRightPadding;
    
    CGFloat imagMaxScale;
    CGFloat imagMin;
    CGFloat imagCurrntScale;
    CGFloat firstX;
    CGFloat firstY;
    
}
@end

@implementation ShowFullImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    imagMinScale = 1.0;
    // Do any additional setup after loading the view.
    
    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    
    UIButton *navLeft = [CommonMethods createNavButtonsWithselectedState:@"cross_showFullImage" normalState:@"cross_showFullImage"];
    navLeft.layer.masksToBounds = NO;
    navLeft.layer.shadowOffset = CGSizeMake(0, -2);
    navLeft.layer.shadowRadius = 2;
    navLeft.layer.shadowOpacity = 0.2;
    UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc]initWithCustomView:navLeft];
    [navLeft setFrame:CGRectMake(0,0,30,30)];
    self.navigationItem.leftBarButtonItem = navLeftButton;
    [navLeft addTarget:self action:@selector(closeButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setLeftBarButtonItems:@[[CommonMethods getNegativeSpacer],navLeftButton]];
    
    navRight = [UIButton buttonWithType:UIButtonTypeCustom];
    NSString *titleForNuber = [NSString stringWithFormat:@"1/%lu",(unsigned long)_imageCount];
    [navRight setTitle:titleForNuber forState:UIControlStateNormal];
    navRight.layer.masksToBounds = NO;
    navRight.layer.shadowOffset = CGSizeMake(0, -2);
    navRight.layer.shadowRadius = 2;
    navRight.layer.shadowOpacity = 0.2;
    UIBarButtonItem *navRightButton = [[UIBarButtonItem alloc]initWithCustomView:navRight];
    [navRight setFrame:CGRectMake(0,0,30,30)];
    self.navigationItem.rightBarButtonItem = navRightButton;
    [self.navigationItem setRightBarButtonItems:@[[CommonMethods getNegativeSpacer],navRightButton]];
    [self createScrollViewWithImageView];
    swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeDown:)];
    [swipeDown setDirection: UISwipeGestureRecognizerDirectionDown];
    [self.view addGestureRecognizer:swipeDown ];
    scrollView.contentOffset = CGPointMake(self.imageTag *scrollView.frame.size.width, 0);
    
    
    UIPinchGestureRecognizer *pinGestureRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handeldPinch:)];
    [imageView addGestureRecognizer:pinGestureRecognizer];
    [imageView setUserInteractionEnabled:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark-
#pragma mark - SwipeGesture
                                           
-(void)handleSwipeDown:(UISwipeGestureRecognizer *)swipe
 {
                                               
   [self dismissViewControllerAnimated:YES completion:nil];
 }
                                           
/**
 Navigation Close Button Action.
 */
-(void)closeButtonAction
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

-(void)scrollViewDidScroll:(UIScrollView *)tempscrollView {
    if ([tempscrollView isEqual:scrollView]) {
        CGPoint offset = scrollView.contentOffset;
        if(offset.x == 0 ) {
            NSString *titleForNuber = [NSString stringWithFormat:@"1/%lu",(unsigned long)_imageCount];
            [navRight setTitle:titleForNuber forState:UIControlStateNormal];
        }
        else if( offset.x <=CGRectGetWidth(self.view.frame)) {
            NSString *titleForNuber = [NSString stringWithFormat:@"2/%lu",(unsigned long)_imageCount];
            [navRight setTitle:titleForNuber forState:UIControlStateNormal];
        }
        else if( offset.x <=CGRectGetWidth(self.view.frame)*2) {
            NSString *titleForNuber = [NSString stringWithFormat:@"3/%lu",(unsigned long)_imageCount];
            [navRight setTitle:titleForNuber forState:UIControlStateNormal];
        }
        else if( offset.x <=CGRectGetWidth(self.view.frame)*3) {
            NSString *titleForNuber = [NSString stringWithFormat:@"4/%lu",(unsigned long)_imageCount];
            [navRight setTitle:titleForNuber forState:UIControlStateNormal];
        }
        else if( offset.x <=CGRectGetWidth(self.view.frame)*4) {
            NSString *titleForNuber = [NSString stringWithFormat:@"5/%lu",(unsigned long)_imageCount];
            [navRight setTitle:titleForNuber forState:UIControlStateNormal];
        }
        
        // Set offset to adjusted value
        scrollView.contentOffset = offset;
    }
}

-(void)createScrollViewWithImageView
{
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height)];
    scrollView.translatesAutoresizingMaskIntoConstraints=NO;
    scrollView.delegate = self;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    [scrollView setPagingEnabled:YES];
    [scrollView setAlwaysBounceVertical:NO];
    [scrollView setAlwaysBounceHorizontal:NO];
    scrollView.delegate=self;
    [scrollView layoutIfNeeded];
    scrollView.bounces = NO;
   
    for (int i = 0; i < self.imageCount; i++)
    {
        xOrigin = i * scrollView.frame.size.width;
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(xOrigin,0, self.view.frame.size.width, self.view.frame.size.height)];
        
         imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView.backgroundColor = [UIColor blackColor];
        [imageView sd_setImageWithURL:[NSURL URLWithString:[self.arrayOfImagesURL objectAtIndex:i]]placeholderImage:[UIImage imageNamed:@"itemProdDefault"]];
        [scrollView addSubview:imageView];

        [imageView setUserInteractionEnabled:YES];
        
        UIPinchGestureRecognizer *pinGestureRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handeldPinch:)];
        
        [imageView addGestureRecognizer:pinGestureRecognizer];
    
    }
    
    scrollView.pagingEnabled  = YES;
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width * self.imageCount,100)];
    [self.view addSubview:scrollView];
    
}
-(void)handeldPinch:(UIPinchGestureRecognizer *)gesture
{
    CGAffineTransform zoomTransform  = CGAffineTransformMakeScale([gesture scale], [gesture scale]);
    [ [gesture view]setTransform:zoomTransform];
    if([gesture scale]< imagMinScale)
    {
        
        gesture.scale = 1.0;
    }
}

-(void)handlePan:(UIPanGestureRecognizer *)recognizer
{
    [UIView beginAnimations:@"ThisAnimation" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDuration:1];
    [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.view cache:YES];
    
    UIView *pannedView = recognizer.view;
    CGPoint transulation = [recognizer translationInView:pannedView.superview];
    pannedView.center = CGPointMake(pannedView.center.x * transulation.x,
                                    pannedView.center.y  * transulation.y);
    [recognizer setTranslation:CGPointZero inView:pannedView.superview];
    
   
}

@end
