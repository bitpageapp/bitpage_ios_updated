//
//  CellForDescNCond.m

//
//  Created by Rahul Sharma on 13/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "CellForDescNCond.h"
#import <QuartzCore/QuartzCore.h>

@implementation CellForDescNCond

- (void)awakeFromNib {
    [super awakeFromNib];
    
//    self.labelTitle.layer.borderWidth = 0.2;
//    self.labelTitle.layer.borderColor = [[UIColor grayColor] CGColor];
//    self.labelForText.layer.borderWidth = 0.2;
//    self.labelForText.layer.borderColor = [[UIColor grayColor] CGColor];
    _descView.layer.borderWidth = 1;
    _descView.layer.borderColor = [[UIColor grayColor] CGColor];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
