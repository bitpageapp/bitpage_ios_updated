//
//  CellForLocationCheck.m

//
//  Created by Rahul Sharma on 13/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "CellForLocationCheck.h"
#import <GoogleMaps/GoogleMaps.h>
@implementation CellForLocationCheck

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)displayLocationName:(ProductDetails *)product
{
    NSString * city = [product.city capitalizedString];
    NSString *countryName = [[NSLocale systemLocale] displayNameForKey:NSLocaleCountryCode value:product.countrySname];
    self.labelForLoc.text = [NSString stringWithFormat:@"%@, %@",city , countryName];
}

-(void)setUpMapView:(double )latitude andLongitude:(double )longi{
    
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude                                                                 longitude:longi zoom:14];
    [self.viewForMap animateToCameraPosition:camera];
    self.viewForMap.myLocationEnabled = NO;
    self.viewForMap.mapType = kGMSTypeNormal;
    self.viewForMap.settings.myLocationButton = NO;
    self.viewForMap.settings.zoomGestures = YES;
    self.viewForMap.settings.tiltGestures = NO;
    self.viewForMap.settings.rotateGestures = NO;
    self.viewForMap.userInteractionEnabled=NO;
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.icon = [UIImage imageNamed:@"itemMapIcon"];
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude , longi);
    //marker.position = CLLocationCoordinate2DMake(latitude, longi);
    marker.map = self.viewForMap;
  
    GMSCircle *circ = [GMSCircle circleWithPosition:position
                                             radius:500];
    circ.fillColor = mBaseColor;
    circ.strokeColor = [UIColor clearColor];
    circ.strokeWidth = 1;
    circ.map = self.viewForMap;
}

@end
